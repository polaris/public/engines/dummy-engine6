import sys
import random

from requests import HTTPError

from utils import save_results


def main(analytics_token):
    score_range = range(1, 5)
    try:
        result = [
            {
                "column1": "Wissenschaftliches Arbeiten I",
                "column2": random.choice(score_range),
            },
            {
                "column1": "Landeskunde I: Studium und Alltag",
                "column2": random.choice(score_range),
            },
            {
                "column1": "Allgemeiner Sprachkurs - B1.1",
                "column2": random.choice(score_range),
            },
            {
                "column1": "Seminar zum Kapitalmarktrecht (SPB 3)",
                "column2": random.choice(score_range),
            },
        ]

        # Send result to rights engine
        save_results(analytics_token, {"result": result})
    except HTTPError as error:
        print(error.response.status_code, error.response.text)


if __name__ == "__main__":
    main(sys.argv[1])
